README for music-production.git
===============================

This repository contains various information about music production and 
is aimed at hobby/semi-professional musicians that want to record and 
produce music at home or in a studio. Today's technology is good enough 
that everyone could get a decent result without paying lots of money in 
a professional studio, but rather record at home or a self-made studio.

Licenses
--------

This depends on the source, but all info in this repository should be 
covered by an open source license that allows for republishing and 
improvement.

### [CC BY-NC-SA 3.0](http://creativecommons.org/licenses/by-nc-sa/3.0/)

- [mixdown.html](mixdown.html)

----

    File ID: f33eab7e-3c31-11e4-b54d-c80aa9e67bbd
    vim: set ts=2 sw=2 sts=2 tw=72 et fo=tcqw fenc=utf8 :
    vim: set com=b\:#,fb\:-,fb\:*,n\:> ft=markdown :
